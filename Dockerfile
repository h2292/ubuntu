# Ubuntu base image
FROM ubuntu:20.04

# Create a directory to mount the git volume
RUN mkdir /git

# Update/upgrade packages
RUN apt update && apt upgrade -y

# Install packages
RUN apt install nano curl wget unzip -y

# Change the working directory to the git directory
WORKDIR /git

# Sleep to keep the container running
ENTRYPOINT sleep 365d