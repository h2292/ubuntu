# ubuntu

This repository allows for easy use of ubuntu within a local kubernetes cluster. It mounts a local git directory so all of your local repositories are available within the pod.

## Running Locally

1. Override the following property in your home directory `~/.kimber/helm-global.yaml`.

``` yaml
localGitDirectory: /c/Users/ssimmons19/git (minikube)
localGitDirectory: /run/desktop/mnt/host/c/Users/sassi/git (WSL)
```

2. Run it via `./gradlew install`.

3. Exec into the pod via something like `kubectl exec -it ubuntu-584b95b465-zp8lt -- bash`.